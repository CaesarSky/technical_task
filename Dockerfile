FROM python:3.9-slim

ENV PYTHONUNBUFFERED 1
ENV POETRY_VERSION=1.5.1

WORKDIR /technical_task

RUN apt-get update -qq && apt-get install -y --no-install-recommends apt-utils curl gnupg2 gcc python3-dev

RUN python3 -m pip install poetry

COPY pyproject.toml .
COPY poetry.lock .
RUN poetry config virtualenvs.create false \
  && poetry install --no-interaction --no-ansi

COPY . .
