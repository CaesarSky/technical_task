from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from apps.user.views import VerifyEmail, RegisterView
from config.yasg import urlpatterns as yasg_urls


v1_api = (
    [
        path("", include("apps.book.urls")),
        path("user/", include("apps.user.urls")),
    ],
    "v1",
)


urlpatterns = [
    path("admin/", admin.site.urls),
    path("email-verify/", VerifyEmail.as_view(), name="email-verify"),
    path("register/", RegisterView.as_view(), name="register"),
    path(
        "api/token/", TokenObtainPairView.as_view(), name="token_obtain_pair"
    ),
    path(
        "api/token/refresh/", TokenRefreshView.as_view(), name="token_refresh"
    ),
    path("api/v1/", include(v1_api, namespace="v1")),
]

urlpatterns += yasg_urls
if settings.DEBUG:
    urlpatterns += static(
        settings.STATIC_URL, document_root=settings.STATIC_ROOT
    )
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
    )
