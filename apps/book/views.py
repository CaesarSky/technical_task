from django.db.models import Avg, FloatField, IntegerField, Count, OuterRef, Exists
from django.db.models.functions import Coalesce
from django_filters.rest_framework import DjangoFilterBackend

from rest_framework import generics, status
from rest_framework.filters import OrderingFilter
from rest_framework import permissions
from rest_framework.response import Response

from apps.book.filters import BookFilter
from apps.book.pagination import CustomPagination
from apps.book.serializers import (
    BookListSerializer, GenreSerializer,
    BookRetrieveSerializer, RatingAddSerializer,
    FavoriteBookSerializer
)
from apps.book.models import Book, FavoriteBook, Genre, Rating


class BookAPIListView(generics.ListAPIView):
    serializer_class = BookListSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    filterset_class = BookFilter
    ordering_fields = (
        "rating",
        "published_date"
    )
    pagination_class = CustomPagination

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            favorites_subquery = FavoriteBook.objects.filter(
                user=user, book=OuterRef('pk')
            )
            queryset = Book.objects.select_related(
                "author", "genre"
            ).prefetch_related(
                "ratings"
            ).annotate(
                rating=Coalesce(Avg("ratings__rating"), 0, output_field=FloatField()),
                is_favorite=Exists(favorites_subquery)
            ).order_by("published_date")
            return queryset
        queryset = Book.objects.select_related(
            "author", "genre"
        ).prefetch_related(
            "ratings"
        ).annotate(
            rating=Coalesce(Avg("ratings__rating"), 0, output_field=FloatField()),
        ).order_by("published_date")
        return queryset


class BookRetrieveAPIView(generics.RetrieveAPIView):
    serializer_class = BookRetrieveSerializer
    lookup_field = "slug"

    def get_queryset(self):
        queryset = Book.objects.select_related(
            "author",
            "genre",
        ).prefetch_related(
            "ratings",
            "ratings__author"
        ).annotate(
            rating=Coalesce(Avg("ratings__rating"), 0, output_field=FloatField()),
            rating_amount=Count("ratings", distinct=True),
        )
        return queryset


class GenreListAPIView(generics.ListAPIView):
    serializer_class = GenreSerializer
    queryset = Genre.objects.all()


class RatingAddAPIView(generics.CreateAPIView):
    serializer_class = RatingAddSerializer
    queryset = Rating.objects.all()
    permission_classes = (permissions.IsAuthenticated,)


class FavoriteBookAddAPIView(generics.GenericAPIView):
    serializer_class = FavoriteBookSerializer
    queryset = FavoriteBook.objects.all()

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
