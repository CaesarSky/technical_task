from django.contrib.auth import get_user_model
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from config.helpers import unique_slugify

User = get_user_model()


class Genre(models.Model):
    title = models.CharField(verbose_name="Наименование", max_length=100)
    slug = models.SlugField(
        'Ссылка на жанр', null=True, unique=True, blank=True
    )

    class Meta:
        verbose_name = "Жанр"
        verbose_name_plural = "Жанры"

    def save(self, **kwargs):
        if self.title and not self.slug:
            unique_slugify(self, self.title)
        super().save(**kwargs)

    def __str__(self):
        return self.title


class Book(models.Model):
    title = models.CharField(verbose_name="Наименование", max_length=100)
    slug = models.SlugField(
        'Ссылка на книгу', null=True, unique=True, blank=True
    )
    description = models.TextField(verbose_name="Описание")
    genre = models.ForeignKey(
        "Genre", verbose_name="Жанр", on_delete=models.SET_NULL,
        related_name="books", null=True
    )
    published_date = models.DateTimeField(
        verbose_name="Дата публикации", auto_now_add=True
    )
    author = models.ForeignKey(
        User, verbose_name="Автор", on_delete=models.CASCADE,
        related_name="books"
    )

    class Meta:
        verbose_name = "Книга"
        verbose_name_plural = "Книги"
        indexes = [
            models.Index(fields=('title', )),
            models.Index(fields=('slug',)),
        ]

    def save(self, **kwargs):
        if self.title and not self.slug:
            unique_slugify(self, self.title)
        super().save(**kwargs)

    def __str__(self):
        return f"{self.title} - {self.author.email}"


class Rating(models.Model):
    book = models.ForeignKey(
        "Book", verbose_name="Книга", on_delete=models.CASCADE,
        related_name="ratings",
    )
    author = models.ForeignKey(
        User, verbose_name="Автор", on_delete=models.SET_NULL,
        related_name="ratings", null=True
    )
    rating = models.PositiveSmallIntegerField(
        validators=[
            MaxValueValidator(5),
            MinValueValidator(1),
        ]
    )
    comment = models.TextField(verbose_name="Описание", null=True, blank=True)

    class Meta:
        verbose_name = "Отзыв"
        verbose_name_plural = "Отзывы"

    def __str__(self):
        return f"{self.author.email} {self.rating}"


class FavoriteBook(models.Model):
    book = models.ForeignKey(
        "Book", verbose_name="Книга", on_delete=models.CASCADE,
        related_name="favorites"
    )
    user = models.ForeignKey(
        User, verbose_name="Пользователь", on_delete=models.CASCADE,
        related_name="favorites"
    )

    class Meta:
        verbose_name = 'Избранная книга'
        verbose_name_plural = 'Избранные книги'
        unique_together = ('book', 'user')
