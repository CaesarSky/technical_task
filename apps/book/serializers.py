from rest_framework import serializers

from apps.book.models import Book, Genre, Rating, FavoriteBook


class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = ("title", "slug")


class BookListSerializer(serializers.ModelSerializer):
    genre = GenreSerializer()
    author = serializers.CharField(source="author.email")
    rating = serializers.IntegerField()
    is_favorite = serializers.BooleanField(default=False)

    class Meta:
        model = Book
        fields = (
            "id",
            "title",
            "slug",
            "is_favorite",
            "genre",
            "author",
            "rating",
            "published_date",
        )


class RatingSerializer(serializers.ModelSerializer):
    author = serializers.CharField(source="author.email")

    class Meta:
        model = Rating
        fields = (
            "author",
            "rating",
            "comment",
        )


class BookRetrieveSerializer(serializers.ModelSerializer):
    genre = GenreSerializer()
    author = serializers.CharField(source="author.email")
    rating = serializers.IntegerField(default=0)
    ratings = RatingSerializer(many=True)
    rating_amount = serializers.IntegerField(default=0)

    class Meta:
        model = Book
        fields = (
            "id",
            "title",
            "genre",
            "author",
            "rating",
            "rating_amount",
            "description",
            "ratings",
            "published_date",
        )


class RatingAddSerializer(serializers.ModelSerializer):

    class Meta:
        model = Rating
        fields = (
            "author",
            "book",
            "rating",
            "comment",
        )
        extra_kwargs = {
            "author": {"read_only": True}
        }

    def create(self, validated_data):
        user = self.context.get("request").user
        if not Rating.objects.filter(author=user, book=validated_data.get("book")).exists():
            rating = Rating.objects.create(
                author=user,
                **validated_data
            )
            return rating
        raise serializers.ValidationError("You already left comment")


class FavoriteBookSerializer(serializers.ModelSerializer):
    class Meta:
        model = FavoriteBook
        fields = (
            "book",
            "user",
        )
        extra_kwargs = {
            "user": {"read_only": True}
        }

    def create(self, validated_data):
        user = self.context.get("request").user
        favorite_product = FavoriteBook.objects.create(
            user=user,
            **validated_data
        )
        return favorite_product
