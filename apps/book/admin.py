from django.contrib import admin

from apps.book.models import Genre, Book, Rating


@admin.register(Genre)
class GenreAdmin(admin.ModelAdmin):
    list_filter = ("title",)
    list_display = ("title", )


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_filter = ("published_date",)
    list_display = ("title", )


@admin.register(Rating)
class RatingAdmin(admin.ModelAdmin):
    list_filter = ("rating",)
    list_display = ("__str__",)
