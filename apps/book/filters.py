from django_filters import rest_framework as filters

from apps.book.models import Book


class BookFilter(filters.FilterSet):
    min_rating = filters.NumberFilter(field_name="rating", lookup_expr='gte')
    max_rating = filters.NumberFilter(field_name="rating", lookup_expr='lte')
    genre = filters.CharFilter(field_name="genre__title", lookup_expr="icontains")
    author = filters.CharFilter(field_name="author__email", lookup_expr="icontains")
    title = filters.CharFilter(field_name="title", lookup_expr="icontains")
    published_date_gte = filters.DateTimeFilter(
        field_name="published_date", lookup_expr="gte"
    )
    published_date_lte = filters.DateTimeFilter(
        field_name="published_date", lookup_expr="lte"
    )

    class Meta:
        model = Book
        fields = [
            "title",
            "min_rating",
            "max_rating",
            "genre",
            "author",
            "published_date_gte",
            "published_date_lte",
        ]
