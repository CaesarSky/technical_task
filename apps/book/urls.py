from django.urls import path

from apps.book.views import (
    BookAPIListView, GenreListAPIView, BookRetrieveAPIView,
    RatingAddAPIView, FavoriteBookAddAPIView
)

urlpatterns = [
    path("books/", BookAPIListView.as_view(), name="books"),
    path("genres/", GenreListAPIView.as_view(), name="genres"),
    path("books/<slug:slug>/", BookRetrieveAPIView.as_view(), name="books-retrieve"),
    path("rating-add/", RatingAddAPIView.as_view(), name="rating-add"),
    path("favorite-book/", FavoriteBookAddAPIView.as_view(), name="favorite-book"),
]
