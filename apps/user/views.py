from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import force_str, DjangoUnicodeDecodeError
from django.utils.http import urlsafe_base64_decode
from rest_framework import views
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from rest_framework_simplejwt.views import TokenObtainPairView

from apps.user.models import CustomUser
from apps.user.serializers import UserSerializer, TokenObtainPairSerializer


class RegisterView(generics.GenericAPIView):
    http_method_names = ["post"]
    serializer_class = UserSerializer

    def post(self, *args, **kwargs):
        serializer = self.get_serializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(is_active=False)
        return Response(
            {"message": "You successfully signed up. Please check you email"},
            status=status.HTTP_201_CREATED,
        )


class VerifyEmail(views.APIView):
    def get(self, request):
        token = request.GET.get("token")
        uidb64 = request.GET.get("uidb64")
        try:
            uid = force_str(urlsafe_base64_decode(uidb64))
            user = CustomUser.objects.get(pk=uid)
            token_generator = PasswordResetTokenGenerator()
            print(token)
            print(user)
            print(uid)
            if token_generator.check_token(user, token):
                user.is_active = True
                user.save()
                # TODO: redirect to frontend page after successfully verification.
                #  I don't have frontend part that's why I'm just printing message to rest framework page
                return Response(
                    {"message": "Email verified successfully"},
                    status=status.HTTP_200_OK,
                )
            else:
                return Response(
                    {"error": "Invalid token1"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
        except (
            TypeError,
            ValueError,
            OverflowError,
            CustomUser.DoesNotExist,
            DjangoUnicodeDecodeError,
        ) as e:
            user = None
            return Response(
                {"error": "Invalid token"}, status=status.HTTP_400_BAD_REQUEST
            )


class EmailTokenObtainPairView(TokenObtainPairView):
    serializer_class = TokenObtainPairSerializer
