import base64

from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.core.mail import EmailMessage
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

User = get_user_model


def generate_verification_token(user: User) -> base64:
    token_generator = PasswordResetTokenGenerator()
    token = token_generator.make_token(user)
    return token


def send_verification_link(
    current_site: str, user_pk: int, token: base64, email: str
) -> None:
    uidb64 = urlsafe_base64_encode(force_bytes(user_pk))
    relative_link = reverse("email-verify")
    print(uidb64)
    print(token)
    absolute_url = f"http://{current_site}{relative_link}?uidb64={uidb64}&token={str(token)}"
    print(absolute_url)
    email_body = (
        f"Hi Use the link below to verify your email \n + {absolute_url}"
    )
    print(email_body)
    email = EmailMessage(
        "Verify your email",
        email_body,
        "noreply@bookstore.com",
        [email],
    )
    email.send(fail_silently=False)
