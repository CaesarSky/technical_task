import base64

from huey.contrib.djhuey import task

from apps.user.service import send_verification_link


@task()
def send_email_verification_link(
    current_site: str, user_pk: int, token: base64, email: str
) -> None:
    send_verification_link(current_site, user_pk, token, email)
