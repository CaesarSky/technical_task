from typing import Dict, Any

from django.contrib.auth import get_user_model
from django.contrib.sites.shortcuts import get_current_site
from rest_framework import serializers
from rest_framework_simplejwt.serializers import (
    TokenObtainPairSerializer as JwtTokenObtainPairSerializer,
)

from apps.user.models import CustomUser
from apps.user.service import generate_verification_token
from apps.user.tasks import send_email_verification_link


class TokenObtainPairSerializer(JwtTokenObtainPairSerializer):
    username_field = get_user_model().USERNAME_FIELD

    def validate(self, attrs: Dict[str, Any]) -> Dict[str, str]:
        try:
            user = CustomUser.objects.get(email=attrs.get("email"))
        except CustomUser.DoesNotExist():
            raise serializers.ValidationError("This user does not exist")
        if user.is_active:
            data = super().validate(attrs)
            return data
        raise serializers.ValidationError("Verify your account")


class UserSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(
        write_only=True,
        required=True,
        help_text="second password should be same like your original password",
        style={"input_type": "password2", "placeholder": "Password"},
    )

    class Meta:
        model = get_user_model()
        fields = ("email", "password", "password2")

    def validate(self, attrs: Dict[str, Any]) -> Dict[str, str]:
        if attrs.get("password") != attrs.get("password2"):
            raise serializers.ValidationError("Your passwords don't match")
        del attrs["password2"]
        return attrs

    def create(self, validated_data):
        user = CustomUser.objects.create(
            email=validated_data.get("email"), is_active=False
        )
        user.set_password(validated_data.get("password"))
        user.save()
        current_site = get_current_site(self.context.get("request")).domain
        token = generate_verification_token(user)
        send_email_verification_link(current_site, user.pk, token, user.email)
        return user
