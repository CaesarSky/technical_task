# technical_task



## Getting started.

Before you get started some things you should know
 - This project works with poetry
 - Prepared for deploy with docker
 - jwt auth

```
git clone https://gitlab.com/CaesarSky/technical_task.git
cd techical_task
create .env file in .envs directory like .env.example
export $(grep -v '^#' .envs/.env | xargs)
python manage.py migrate
python manage.py runserver
python manage.py createsuperuser

or you can run in docker: 
same prepare .env file before you get started
docker compose up --build -d
```
